import {
    SET_ALL_MESSAGES,
    SET_MESSAGE,
    DELETE_MESSAGE,
    LIKE_MESSAGE,
    EDIT_MESSAGE,
    SET_MESSAGE_TO_EDIT
} from './actionTypes';

export default (state = { messages: [], newId: 1, messageToEdit: undefined }, action) => {
    switch (action.type) {
        case SET_ALL_MESSAGES:
            return {
                ...state,
                messages: action.messages
            };
        case SET_MESSAGE:
            return {
                ...state,
                messages: [...state.messages, action.message],
                newId: state.newId + 1
            };
        case DELETE_MESSAGE:
            return {
                ...state,
                messages: state.messages.filter(message => message.id !== action.id)
            };
        case LIKE_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(m => {
                    if (m.id == action.id)
                        m.isLike = !m.isLike;
                    return m;
                })
            };
        case EDIT_MESSAGE:
            return {
                ...state,
                messages: state.messages.map(m => (m.id === action.message.id ? action.message : m))
            };
        case SET_MESSAGE_TO_EDIT:
            return {
                ...state,
                messageToEdit: action.message
            };
        default:
            return state;
    }
};
