import React from 'react';
import { connect } from 'react-redux';

import Header from '../../components/Header';
import MessageList from '../../components/MessageList';
import MessageInput from '../../components/MessageInput';
import Spinner from '../../components/Spinner';
import EditModal from '../../components/EditModal';

import {
  loadMessages,
  sendMessage,
  deleteMessage,
  likeMessage,
  editMessage,
  toggleEditModal
} from './actions';

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.sendMessage = this.sendMessage.bind(this);
  }

  sendMessage(message) {
    let newMessage = {
      id: '' + this.props.newId,
      userId: "1",
      avatar: '',
      user: "Me",
      text: message,
      createdAt: new Date().toISOString(),
      editedAt: ""
    }

    this.props.sendMessage(newMessage);
  }

  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then(response => response.json())
      .then(data => {
        data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt))
          .map(m => {
            m['isLike'] = false;
            return m;
          });
        this.props.loadMessages(data);
      });
  }

  render() {
    return (
      <>
        {
          this.props.messages.length !== 0 ?
            (
              <div className="chat">
                <Header messages={this.props.messages} />
                <MessageList
                  messages={this.props.messages}
                  deleteMessage={this.props.deleteMessage}
                  likeMessage={this.props.likeMessage}
                  toggleEditModal={this.props.toggleEditModal}
                />
                <MessageInput sendMessage={this.sendMessage} />
                {this.props.messageToEdit &&
                  <EditModal
                    messageToEdit={this.props.messageToEdit}
                    editMessage={this.props.editMessage}
                    toggleEditModal={this.props.toggleEditModal}
                  />}
              </div>
            ) :
            <Spinner />
        }
      </>
    )
  }
}

const mapStateToProps = rootState => ({
  messages: rootState.chat.messages,
  newId: rootState.chat.newId,
  messageToEdit: rootState.chat.messageToEdit
});

const mapDispatchToProps = {
  loadMessages,
  sendMessage,
  deleteMessage,
  likeMessage,
  editMessage,
  toggleEditModal
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
