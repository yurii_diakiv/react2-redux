import {
    SET_ALL_MESSAGES,
    SET_MESSAGE,
    DELETE_MESSAGE,
    LIKE_MESSAGE,
    EDIT_MESSAGE,
    SET_MESSAGE_TO_EDIT
} from './actionTypes';

export const sendMessage = message => ({
    type: SET_MESSAGE,
    message
});

export const loadMessages = messages => ({
    type: SET_ALL_MESSAGES,
    messages
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    id
});

export const likeMessage = id => ({
    type: LIKE_MESSAGE,
    id
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    message
});

export const toggleEditModal = message => ({
    type: SET_MESSAGE_TO_EDIT,
    message
});
