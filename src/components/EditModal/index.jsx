import React from 'react';

class EditModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.messageToEdit.text
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSaveClick = this.handleSaveClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
  }

  handleChange(event) {
    this.setState({
      ...this.state,
      value: event.target.value
    });
  }

  handleSaveClick() {
    if (this.state.value !== '') {
      let newMessage = {};
      Object.assign(newMessage, this.props.messageToEdit);
      newMessage.text = this.state.value;
      newMessage.editedAt = new Date().toISOString();

      this.props.editMessage(newMessage);
      this.setState({
        ...this.state,
        value: ''
      });
      this.props.toggleEditModal();
    }
  }

  handleCancelClick() {
    this.props.toggleEditModal();
  }

  render() {
    return (
      <div className="modal" style={{ display: "block"  } } tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{ padding: "5px" }}>
            <div className="modal-header">
              <h5 className="modal-title">Edit message</h5>
            </div>
            <div className="modal-body">
              <div className="form-group row">
                <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={this.state.value} onChange={this.handleChange}></textarea>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={this.handleCancelClick}>Cancel</button>
              <button className="btn btn-primary" onClick={this.handleSaveClick}>Save</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditModal;