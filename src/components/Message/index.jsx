import React from 'react';
import { Heart, HeartFill, Trash, Gear } from 'react-bootstrap-icons';

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleLikeClick = this.handleLikeClick.bind(this);
    this.toggleEditModal = this.toggleEditModal.bind(this);
  }

  handleDeleteClick() {
    this.props.deleteMessage(this.props.m.id);
  }

  handleLikeClick() {
    this.props.likeMessage(this.props.m.id);
  }

  toggleEditModal() {
    this.props.toggleEditModal(this.props.m);
  }

  render() {
    return (
      <div className="message">
        <div className="message-main">
          {this.props.m.avatar && <img src={this.props.m.avatar} />}
          <p>{this.props.m.text}</p>
        </div>
        <div className="message-secondary">
        <p style={{fontSize : '13.5px'}}>{new Date(this.props.m.createdAt).toLocaleString()}</p>
          {this.props.m.userId === '1' &&
            <Trash className="icon-button" onClick={this.handleDeleteClick} />}
          {this.props.m.userId === '1' && this.props.isLast &&
            <Gear className="icon-button edit-button" onClick={this.toggleEditModal} />}
          {this.props.m.userId !== '1' &&
            (this.props.m.isLike === true ? 
            <HeartFill className="icon-button" onClick={this.handleLikeClick} /> : 
            <Heart className="icon-button" onClick={this.handleLikeClick} />)}
        </div>
      </div>
    );
  }
}

export default Message;