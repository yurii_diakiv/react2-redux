import React from 'react';

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <p>My chat</p>
        <p>{new Set(this.props.messages.map(m => m.userId)).size} participants</p>
        <p>{this.props.messages.length} messages</p>
        <p style={{marginLeft: 'auto'}}>Last message at {new Date(this.props.messages[this.props.messages.length - 1]?.createdAt).toLocaleString()}</p>
      </div>
    );
  }
}

export default Header;