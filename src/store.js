import { createStore, combineReducers } from 'redux';
import chatReducer from './containers/Chat/reducer';

const initialState = {};

const reducers = {
    chat: chatReducer
};

const rootReducer = combineReducers({
    ...reducers
});

const store = createStore(
    rootReducer,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;